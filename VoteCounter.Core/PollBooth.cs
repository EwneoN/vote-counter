﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace VoteCounter.Core
{
	public class PollBooth
	{
		private readonly Dictionary<string, IVoteCounter> _Counters;

		public PollBooth(IVoteCounterFactory voteCounterFactory, string[] candidates)
		{
			if (voteCounterFactory == null)
			{
				throw new ArgumentNullException(nameof(voteCounterFactory));
			}

			if (candidates == null)
			{
				throw new ArgumentNullException(nameof(candidates));
			}

			if (candidates.Length == 0)
			{
				throw new ArgumentException("Value cannot be an empty collection.", nameof(candidates));
			}

			_Counters = candidates
				.ToDictionary(candidate => candidate.ToLower(), candidate => voteCounterFactory.Create(candidate));
		}

		public void VoteForCandidate(string candidate)
		{
			if (candidate == null)
			{
				throw new ArgumentNullException(nameof(candidate));
			}

			if (!_Counters.TryGetValue(candidate.ToLower(), out IVoteCounter counter))
			{
				throw new KeyNotFoundException($"{candidate} is not a valid candidate");
			}

			counter.Vote();
		}

		public int GetVotesForCandidate(string candidate)
		{
			if (candidate == null)
			{
				throw new ArgumentNullException(nameof(candidate));
			}

			if (!_Counters.TryGetValue(candidate.ToLower(), out IVoteCounter counter))
			{
				throw new KeyNotFoundException($"{candidate} is not a valid candidate");
			}

			return counter.GetVotes();
		}

		public IOrderedEnumerable<VoteCount> GetVoteCounts()
		{
			return _Counters
				.Select(c => new VoteCount(c.Value))
				.OrderByDescending(c => c.Votes);
		}
	}
}
