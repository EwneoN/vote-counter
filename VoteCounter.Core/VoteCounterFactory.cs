﻿namespace VoteCounter.Core
{
	public class VoteCounterFactory : IVoteCounterFactory
	{
		public IVoteCounter Create(string candidate)
		{
			return new VoteCounter(candidate);
		}
	}
}