﻿using System;

namespace VoteCounter.Core
{
	public class VoteCount
	{
		public string Candidate { get; }
		public int Votes { get; }

		public VoteCount(IVoteCounter counter)
		{
			if (counter == null)
			{
				throw new ArgumentNullException(nameof(counter));
			}

			Candidate = counter.Candidate;
			Votes = counter.GetVotes();
		}
	}
}
