﻿using System.Collections.Generic;

namespace VoteCounter.Core
{
	public class VoteCountComparer : IComparer<VoteCount>
	{
		public int Compare(VoteCount x, VoteCount y)
		{
			return x.Votes.CompareTo(y.Votes);
		}
	}
}
