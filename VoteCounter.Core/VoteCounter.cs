﻿namespace VoteCounter.Core
{
	public class VoteCounter : VoteCounterBase
	{
		private int _Votes;

		public VoteCounter(string candidate) : base(candidate) { }
		
		protected override void VoteInner()
		{
			_Votes++;
		}

		protected override int GetVotesInner()
		{
			return _Votes;
		}
	}
}
