﻿namespace VoteCounter.Core
{
	public interface IVoteCounter
	{
		string Candidate { get; }
		void Vote();
		int GetVotes();
	}
}