﻿using System;

namespace VoteCounter.Core
{
	public abstract class VoteCounterBase : IVoteCounter
	{
		private readonly object _VoteLocker;

		public string Candidate { get; }

		protected VoteCounterBase(string candidate)
		{
			if (string.IsNullOrWhiteSpace(candidate))
			{
				throw new ArgumentException("Value cannot be null or whitespace.", nameof(candidate));
			}

			Candidate = candidate;

			_VoteLocker = new object();
		}

		public void Vote()
		{
			lock (_VoteLocker)
			{
				VoteInner();
			}
		}

		public int GetVotes()
		{
			lock (_VoteLocker)
			{
				return GetVotesInner();
			}
		}

		protected abstract void VoteInner();
		protected abstract int GetVotesInner();
	}
}