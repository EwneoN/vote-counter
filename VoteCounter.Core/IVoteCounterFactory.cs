﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VoteCounter.Core
{
	public interface IVoteCounterFactory
	{
		IVoteCounter Create(string candidate);
	}
}
