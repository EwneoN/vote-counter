﻿using System;
using System.Linq;
using FluentAssertions;
using Xunit;

namespace VoteCounter.Core.Tests
{
	public class PollBoothTest
	{
		private const string _Candidate1 = "Candidate 1";
		private const string _Candidate2 = "Candidate 2";
		private const string _Candidate3 = "Candidate 3";

		[Theory]
		[InlineData(_Candidate1, 1)]
		[InlineData(_Candidate2, 10)]
		[InlineData(_Candidate3, 100)]
		public void VotingForCandidateShouldIncrementCountForCandidate(string candidate, int numberOfTimesToVote)
		{
			// Arrange
			IVoteCounterFactory factory = new VoteCounterFactory();
			PollBooth pollBooth = new PollBooth(factory, new[] { _Candidate1, _Candidate2, _Candidate3 });
			int votesForCandidate = 0;

			// Act
			Action act = () =>
			{
				for (int i = 0; i < numberOfTimesToVote; i++)
				{
					pollBooth.VoteForCandidate(candidate);
				}

				votesForCandidate = pollBooth.GetVotesForCandidate(candidate);
			};

			// Assert
			act.Should()
				.NotThrow();

			votesForCandidate
				.Should()
				.BeGreaterThan(0)
				.And
				.Be(numberOfTimesToVote);
		}

		[Fact]
		public void TotalVotesShouldMatchNumberOfTimesVotedForEachCandidate()
		{
			// Arrange
			int candidate1Votes = 100;
			int candidate2Votes = 50;
			int candidate3Votes = 10;
			IVoteCounterFactory factory = new VoteCounterFactory();
			PollBooth pollBooth = new PollBooth(factory, new[] { _Candidate1, _Candidate2, _Candidate3 });
			IOrderedEnumerable<VoteCount> votesForCandidates = null;
			VoteCount first = null;
			VoteCount second = null;
			VoteCount third = null;

			// Act
			Action act = () =>
			{
				for (int i = 0; i < candidate1Votes; i++)
				{
					pollBooth.VoteForCandidate(_Candidate1);
				}
				for (int i = 0; i < candidate2Votes; i++)
				{
					pollBooth.VoteForCandidate(_Candidate2);
				}
				for (int i = 0; i < candidate3Votes; i++)
				{
					pollBooth.VoteForCandidate(_Candidate3);
				}

				votesForCandidates = pollBooth.GetVoteCounts();

				first = votesForCandidates.ElementAt(0);
				second = votesForCandidates.ElementAt(1);
				third = votesForCandidates.ElementAt(2);
			};

			// Assert
			act.Should()
				.NotThrow();

			votesForCandidates
				.Should()
				.NotBeNullOrEmpty()
				.And
				.BeInDescendingOrder(new VoteCountComparer());

			first
				.Should()
				.NotBeNull();

			first.Candidate
				.Should()
				.NotBeNullOrWhiteSpace()
				.And
				.Be(_Candidate1);

			first.Votes
				.Should()
				.BeGreaterThan(0)
				.And
				.Be(candidate1Votes);

			second
				.Should()
				.NotBeNull();

			second.Candidate
				.Should()
				.NotBeNullOrWhiteSpace()
				.And
				.Be(_Candidate2);

			second.Votes
				.Should()
				.BeGreaterThan(0)
				.And
				.Be(candidate2Votes);

			third
				.Should()
				.NotBeNull();

			third.Candidate
				.Should()
				.NotBeNullOrWhiteSpace()
				.And
				.Be(_Candidate3);

			third.Votes
				.Should()
				.BeGreaterThan(0)
				.And
				.Be(candidate3Votes);
		}
	}
}
