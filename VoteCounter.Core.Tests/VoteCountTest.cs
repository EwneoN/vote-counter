using System;
using FluentAssertions;
using Xunit;

namespace VoteCounter.Core.Tests
{
	public class VoteCountTest
	{
		private const string _Candidate1 = "Candidate 1";
		private const string _Candidate2 = "Candidate 2";
		private const string _Candidate3 = "Candidate 3";

		[Theory]
		[InlineData(_Candidate1, 1)]
		[InlineData(_Candidate2, 10)]
		[InlineData(_Candidate3, 0)]
		public void VotingShouldIncrementCount(string candidate, int numberOfTimesToVote)
		{
			// Arrange
			VoteCounter counter = new VoteCounter(candidate);
			int numberOfVotes = 0;

			// Act
			Action act = () =>
			{
				for (int i = 0; i < numberOfTimesToVote; i++)
				{
					counter.Vote();
				}

				numberOfVotes = counter.GetVotes();
			};

			// Assert
			act
				.Should()
				.NotThrow();

			counter.Candidate
				.Should()
				.NotBeNullOrWhiteSpace()
				.And
				.Be(candidate);

			numberOfVotes
				.Should()
				.BeGreaterOrEqualTo(0)
				.And
				.Be(numberOfTimesToVote);
		}
	}
}
